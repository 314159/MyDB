package com.jeecg;

import java.util.List;
import java.util.Map;

import com.jeecg.bean.Person;

public class TestMiniMongoDb {
	
	private static final String uuid  = "bc339b82-305c-4dca-972e-bccad822f80e";
	private static final String database_name = "mini-mongodb.xml";
	
	public static void main(String[] args) {
		new TestMiniMongoDb().createDatabase();
		new TestMiniMongoDb().insertData();
		new TestMiniMongoDb().updateData();
		new TestMiniMongoDb().listAll();
	}
	
	/**
	 * ������ݿ�
	 */
	public void createDatabase() {
		MiniMongoDb dao = new MiniMongoDb();
		try {
			dao.createDataBase(database_name);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * �������
	 */
	public void insertData() {
		Person po = new Person();
		po.setName("lisan");
		po.setAge(20);
		po.setMoney(2000.98);
		po.setSex("男");
		try {
			MiniMongoDb dao = new MiniMongoDb();
			dao.addData(database_name, "test", po);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * �޸����
	 */
	public void updateData() {
		Person po = new Person();
		po.set_uuid(uuid);
		po.setName("����");
		po.setAge(24);
		po.setMoney(1500.00);
		try {
			MiniMongoDb dao = new MiniMongoDb();
			dao.updataData(database_name, "test", po);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * ɾ�����
	 */
	public void deleteData() {
		Person po = new Person();
		po.set_uuid(uuid);
		try {
			MiniMongoDb dao = new MiniMongoDb();
			dao.deleteData(database_name, "test", po);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void listAll() {
		try {
			MiniMongoDb dao = new MiniMongoDb();
			List<Map> list = dao.loadTableDatas(database_name, "test");
			System.out.println(list.toString());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}	





